/**
 * Convert rgb colour to hexadecimal
 * @param r
 * @param g
 * @param b
 * @returns {string}
 */
function convertRGBToHex(r, g, b) {
    return ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

/**
 * Get the average colour of an array of colours passed in
 * @param imageDataArray
 * @returns {string}
 */
function getAverageColourHex(imageDataArray) {
    var totalR = 0, totalG = 0, totalB = 0;
    var numPixels = imageDataArray.length / 4;

    for(var i = 0; i < imageDataArray.length; i+=4) {
        totalR += imageDataArray[i];
        totalG += imageDataArray[i+1];
        totalB += imageDataArray[i+2];
    }

    return convertRGBToHex(
        Math.round(totalR / numPixels),
        Math.round(totalG / numPixels),
        Math.round(totalB / numPixels));
}

/**
 * When the worker receives a message, calculate the average colour
 * @param e
 */
onmessage = function(e) {
    var imageDataArray = e.data[0];
    var promiseCurrRow = e.data[1];
    var promiseCurrCol = e.data[2];

    var colour = getAverageColourHex(imageDataArray);
    postMessage([colour, promiseCurrRow, promiseCurrCol]);
};